<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class CheckKasir
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = Auth::user();
        if(empty($user)){
             return redirect('/');
        } else if (Auth::user()->level == 'kasir'){
            return $next($request);
        } else if(Auth::user()->level == 'pelayan'){
            return redirect('/pelayan');
        } else {

        }
    }
}
