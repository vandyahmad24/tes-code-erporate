<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert(
            [
                'name' => 'aku kasir',
                'email' => 'kasir@gmail.com',
                'password' => bcrypt('12341234'),
                'level' => 'kasir',
            ]
        );
         DB::table('users')->insert(
            [
                'name' => 'aku pelayan',
                'email' => 'pelayan@gmail.com',
                'password' => bcrypt('12341234'),
                'level' => 'pelayan',
            ]
        );
    }
}
