<?php

use Illuminate\Database\Seeder;


class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('menus')->insert(
            [
                'name' => 'bakso',
                'harga' => '10000',
                'tipe' => 'makanan',
                'status' => 'noready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'nasi goreng',
                'harga' => '9000',
                'tipe' => 'makanan',
                'status' => 'noready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'nasi ayam',
                'harga' => '9000',
                'tipe' => 'makanan',
                'status' => 'ready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'nasi ikan',
                'harga' => '15000',
                'tipe' => 'makanan',
                'status' => 'ready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'Es Teh',
                'harga' => '2500',
                'tipe' => 'minuman',
                'status' => 'ready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'Es Jeruk',
                'harga' => '3000',
                'tipe' => 'minuman',
                'status' => 'ready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'Jus Mangga',
                'harga' => '7000',
                'tipe' => 'minuman',
                'status' => 'noready',
            ]
        );
        DB::table('menus')->insert(
            [
                'name' => 'Es Buah',
                'harga' => '15000',
                'tipe' => 'minuman',
                'status' => 'noready',
            ]
        );

    }
}
