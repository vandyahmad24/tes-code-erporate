<?php

namespace Modules\Pelayan\Entities;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['nopesanan','nomeja','total','user_id','status'];

   public function order_detail()
    {
    	return $this->hasMany("Modules\Pelayan\Entities\Order_detail");
    }
}
