<?php

namespace Modules\Pelayan\Entities;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['name','harga','tipe','status'];
    public function order_detail()
    {
    	return $this->hasMany("Modules\Pelayan\Entities\Order_detail");
    }
}
