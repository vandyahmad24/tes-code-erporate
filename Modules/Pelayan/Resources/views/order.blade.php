@extends('pelayan::layouts.template')
@section('title', 'Halaman Pelayan')
@section('content')
 	<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline" id="form-update">
						<div class="panel-heading" id="form-addorder">
							<h3 class="panel-title">Buat Pesanan</h3>
						</div>
						<div class="panel-body">
							<form action="{{url('pelayan/order/add')}}" method="POST" autocomplete="off" id="form-input">
							  <div class="form-group">
							    <label for="nopesanan">Nomor Pesanan</label>
							    <input type="text" name="nopesanan" class="form-control" id="nopesanan" readonly value="{{$date}}-{{$nopesanan}}">
							  </div>
							  <div class="form-group">
							    <label for="nomeja">Nomor Meja</label>
							    <select class="form-control" name="nomeja" id="nomeja">
									  <option value="1">Meja 1</option>
									  <option value="2">Meja 2</option>
									  <option value="3">Meja 3</option>
									  <option value="4">Meja 4</option>
							    </select>
							  </div>
							  <input type="hidden" id="status" name="status" value="active">
							  <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }} ">
							  @csrf
							  <button type="button" class="btn btn-primary" id="simpan">Submit</button>
							</form>
						</div>
					</div>
					<div class="panel panel-headline" id="keterangan">
						<form action="{{url('pelayan/order/detailorder')}}" method="POST" autocomplete="off" id="form-detail">
						@csrf
						<div class="panel-heading" id="judul-keterangan">

						</div>
						<hr>
						<div class="panel-body" id="detailorder">
							<div class="form-group">
							<label for="menu_id">Choose Your Food</label>
							<select class="form-control text-uppercase menu_id" name="menu_id[]">
							  <option>Silahkan Pilih pesanan</option>
							  @foreach($menu as $m)
							  <option value="{{$m->id}}">{{$m->name}} </option>
							  @endforeach
							</select>
							<input type="text" name="qty[]" class="form-control qty" placeholder="Silahkan Masukkan Jumlah Menu" required>
							
							</div>
							<div id="tambahorder">
								
							</div>
							 <button type="button" class="btn btn-success" id="tambah">Tambah Pesanan</button>
							 <button type="button" class="btn btn-primary" id="simpanorder">Submit</button>
						</div>
						</form>
					</div>
					<!-- END OVERVIEW -->
					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>

<!-- Jquery Untuk Ajax -->
<script type="text/javascript">
      //script untk paginate
	$(document).ready(function() 
	  {
	  	$("#keterangan").hide();
	    $("#simpan").click(function() 
	    {
	      $("#simpan").remove();
	      $.ajax({
	      	url:"{{url('pelayan/order/add')}}",
	    	method:"POST",
	    	data:$("#form-input").serialize(),
	    	success:function(data){
	    		$("#keterangan").show();
	    		$("#form-update").remove();
	    		var obj = jQuery.parseJSON(JSON.stringify(data));
                 $("#judul-keterangan").append("<h3> No Pesanan "+obj.nopesanan+ "</h3>");
                 $("#judul-keterangan").append("<h3> No Meja "+obj.nomeja+ "</h3>");
                 $("#judul-keterangan").append("<input type='hidden' name='order_id' id='order_id' value='"+obj.id+ "'></input>");
	    	}
	      });
	    });
	    $("#tambah").click(function(){
	    	$("#tambahorder").append(`<div class="form-group">
							<label for="menu_id">Silahkan Pilih Menu</label>
							<select class="form-control text-uppercase menu_id" name="menu_id[]">
							  <option>Silahkan Pilih pesanan</option>
							  @foreach($menu as $m)
							  <option value="{{$m->id}}">{{$m->name}}</option>
							  @endforeach
							</select>
							<input type="text" name="qty[]" class="form-control qty" placeholder="Silahkan Masukkan Jumlah Menu">
							</div>`);
	    });
	    $("#simpanorder").click(function(){
	    	$.ajax({
	    		url:"{{url('pelayan/order/detailorder')}}",
	    		method:"POST",
	    		data:$("#form-detail").serialize(),
	    		success:function(result){
	    			var obj = jQuery.parseJSON(JSON.stringify(result));
	    			alert("order telah berhasil di buat dengan total "+obj.total);
	    			window.location.replace("/pelayan/pesanan/daftar");

	    		}
	    	})
	    });
	});                            
</script>
<!-- End Jquery -->
@endsection
