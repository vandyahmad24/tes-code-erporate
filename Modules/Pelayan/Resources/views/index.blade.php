@extends('pelayan::layouts.template')
@section('title', 'Halaman Pelayan')
@section('content')
 	<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Laporan</h3>
							<p class="panel-subtitle">Login Tanggal {{$waktu}}</p>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="lnr lnr-cart"></i></span>
										<p>
											<span class="number">{{$order}}</span>
											<span class="title">Jumlah Order</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-line-chart"></i></span>
										<p>
											<span class="number">IDR {{number_format($totalpendapatan,0,"",".")}} </span>
											<span class="title">Total Pendapatan</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bell"></i></span>
										<p>
											<span class="number">{{$menu}}</span>
											<span class="title">Total Menu yang Aktif</span>
										</p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="metric">
										<span class="icon"><i class="fa fa-bar-chart"></i></span>
										<p>
											<span class="number">35%</span>
											<span class="title">Conversions</span>
										</p>
									</div>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<h3>Daftar Menu yang Ready</h3>
									 <table class="table table-striped table-bordered">
									    <thead>
									      <tr>
									        <th>Nama Menu</th>
									        <th>Harga</th>
									        <th>Tipe</th>
									      </tr>
									    </thead>
									    <tbody>
									    	 @foreach($menuready as $m)
									      <tr>
									        <td>{{$m->name}}</td>
									        <td>{{$m->harga}}</td>
									        <td>{{$m->tipe}}</td>
									      </tr>
									      @endforeach
									    </tbody>
									  </table>
								</div>
								<div class="col-md-6">
									<h3>Daftar Menu yang Tidak Ready</h3>
									 <table class="table table-striped table-bordered">
									    <thead>
									      <tr>
									        <th>Nama Menu</th>
									        <th>Harga</th>
									        <th>Tipe</th>
									      </tr>
									    </thead>
									    <tbody>
									    	 @foreach($menunonready as $m)
									      <tr>
									        <td>{{$m->name}}</td>
									        <td>{{$m->harga}}</td>
									        <td>{{$m->tipe}}</td>
									      </tr>
									      @endforeach
									    </tbody>
									  </table>
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
@endsection
