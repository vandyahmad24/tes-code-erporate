<form action="/pelayan/pesanan/daftar/edit/{{$order->id}}" method="POST" autocomplete="off" id="form-edit">
 @csrf
 <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Edit Pesanan</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
   </div>
   <div class="modal-body">
   	<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
        <div class="form-group">
		<label for="nopesanan">Nomer Pesanan</label>
		<input type="text" name="nopesanan" class="form-control" value="{{$order->nopesanan}}" readonly>
		</div>
		<div class="form-group">
	    <label for="nomeja">Nomor Meja</label>
	    <select class="form-control" name="nomeja" id="nomeja">
			  <option value="1">Meja 1</option>
			  <option value="2">Meja 2</option>
			  <option value="3">Meja 3</option>
			  <option value="4">Meja 4</option>
	    </select>
	  </div>
		@foreach($order->order_detail as $detail)
		<input type="hidden" name="order_id[]" value="{{$detail->order_id}}">
		<div class="form-group">
		<label for="nopesanan">Nama Menu</label>
		<select class="form-control text-uppercase menu_id" name="menu_id[]">
		  <option value="{{$detail->menu_id}}">{{$detail->menu->name}}</option>
		  @foreach($menu as $m)
		  <option value="{{$m->id}}">{{$m->name}} </option>
		  @endforeach
		</select>	
		<input type="text" class="form-control" value="{{$detail->qty}}" name="qty[]" >
		</div>
		@endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary simpan">Simpan Perubahan</button>
      </div>
</form>


<script type="text/javascript">
      //script untk paginate
	$(document).ready(function(){
		$('.simpan').click(function(){
			$('.simpan').remove();
			$.ajax({
				url:"/pelayan/pesanan/daftar/edit/{{$order->id}}",
		    	method:"POST",
		    	data:$("#form-edit").serialize(),
		    	success:function(data){
		    		alert('sukses');
		    	}
			});
		});
	});


