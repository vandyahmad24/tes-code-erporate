@extends('pelayan::layouts.template')
@section('title', 'Halaman Pelayan')
@section('content')
 	<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline" id="form-update">
						<div class="panel-heading" id="form-addorder">
							<h2 class="panel-title">Daftar Pesanan</h2>
							<hr>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col">
									 <table class="table table-striped table-bordered">
									    <thead>
									      <tr>
									        <th>No Pesanan</th>
									        <th>Nomer Meja</th>
									        <th>Menu yang dipesan</th>
									        <th>Jumlah yang dipesan</th>
									        <th>total</th>
									        <th>Action</th>
									      </tr>
									    </thead>
									    <tbody>
									    	 @foreach($menu as $m)
									      <tr>
									        <td>{{$m->nopesanan}}</td>
									        <td>{{$m->nomeja}}</td>
									        <td class="text-uppercase">
									        	@foreach($m->order_detail as $detail)
									        	<li>
									        		{{$detail->menu->name}}
									        	</li>
									        	@endforeach
									        </td>
									        <td class="text-uppercase">
									        	@foreach($m->order_detail as $detail)
									        	<li>
									        		{{$detail->qty}}
									        	</li>
									        	@endforeach
									        </td>
									        <td>IDR {{number_format($m->total,0,"",".")}}</td>
									        <td>
									        <!-- 	<a  href="/pelayan/pesanan/daftar/edit/{{$m->id}}" class="btn btn-danger edit"> Edit </a> -->
									        	<a href="/pelayan/pesanan/daftar/edit/{{$m->id}}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">
												 Edit
												</a>
									        </td>
									      </tr>
									      @endforeach
									    </tbody>
									  </table>
								</div>
							</div>
						</div>
					</div>
					<!-- END OVERVIEW -->
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
     
     
    </div>
  </div>
</div>		
@endsection
