<?php

namespace Modules\Pelayan\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use Modules\Pelayan\Entities\Order;
use Modules\Pelayan\Entities\Logactivity;
use Modules\Pelayan\Entities\Menu;
use Modules\Pelayan\Entities\Order_detail;
class PelayanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $waktu = Carbon::now()->format('d-m-Y');
        $order = Order::count();
        $totalpendapatan = Order::sum('total');
        $menu = Menu::where('status','ready')->count();
        $menuready = Menu::where('status','ready')->get();
        $menunonready = Menu::where('status','noready')->get();
        return view('pelayan::index',compact('waktu','totalpendapatan','menu','order','menuready','menunonready'));
    }
    public function daftarpesanan()
    {
        $menu = Order::where('status','active')->orderBy('created_at', 'desc')->get();

        return view('pelayan::daftar',compact('menu'));
    }
     public function order()
    {
        $waktu = Carbon::now();
        $date = "ERP".date('dmy', strtotime($waktu));
        $count = Order::all()->count()+1;
        $nopesanan = sprintf("%03d", $count);
        $menu = Menu::where('status','ready')->get();
        return view('pelayan::order',compact('date','nopesanan','menu'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        $order->nopesanan = $request->nopesanan;
        $order->nomeja = $request->nomeja;
        $order->user_id = $request->user_id;
        $order->total = 0;
        $order->status = $request->status;
        $order->save();
       
        $logactivity = New Logactivity;
        $logactivity->user_id = $request->user_id;
        $logactivity->aktivityas = "Menambahkan pesanan dgn no pesanan ".$request->nopesanan;
        $logactivity->save();
        return response()->json([
            'id' => $order->id,
            'nopesanan' => $order->nopesanan,
            'nomeja' => $order->nomeja,
         ]);
    }
    public function pesanan()
    {
        $menu = Menu::where('status','ready')->get();
        return view('pelayan::loadorder',compact('menu'));
    }
    public function detailorder(Request $request)
    {   
        $menus = $request->menu_id;
        foreach ($request->menu_id as $key => $value) {
            $harga = Menu::find($value);
            $subharga = $harga->harga;
            // dd($subharga);
            $orders = $request->order_id;
            $qtys = $request->qty;
        
                $data = array(
                    'menu_id' => $menus[$key],
                    'order_id'=>$orders,
                    'qty'=>$qtys[$key],
                    'subtotal'=>$subharga*$qtys[$key]
                );
                Order_detail::insert($data);
        }

        $caritotal = Order_detail::where('order_id',$request->order_id)->sum('subtotal');
        $masukorder = Order::find($request->order_id);
        $masukorder->total = $caritotal;
        $masukorder->save();
        return response()->json([
            'total' => $masukorder->total,
         ]);


    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('pelayan::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $order =  Order::find($id);
        $menu = Menu::where('status','ready')->get();
        return view('pelayan::edit',compact('order','menu'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->nopesanan = $request->nopesanan;
        $order->nomeja= $request->nomeja;
        $menus = $request->menu_id;
        foreach ($menus as $menu => $value) {
             $harga = Menu::find($value);
             $subharga = $harga->harga;
             $orders = $request->id;
             $qtys = $request->qty;
             $data = array(
                    'menu_id' => $menus[$menu],
                    'order_id'=>$orders,
                    'qty'=>$qtys[$menu],
                    'subtotal'=>$subharga*$qtys[$menu]
                );
                Order_detail::insert($data);
                $caritotal = Order_detail::where('order_id',$request->id)->sum('subtotal');
                $masukorder = Order::find($request->id);
                $masukorder->total = $caritotal;
                $masukorder->save();
                return response()->json([
                    'total' => $masukorder->total,
                 ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
