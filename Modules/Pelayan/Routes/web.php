<?php
use App\Http\Middleware\CheckPelayan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('pelayan')->group(function() {
//     Route::get('/', 'PelayanController@index');
// })->middleware('checkpelayan');

Route::group(['prefix' => 'pelayan', 'middleware' => ['checkpelayan']], function()
{
	 Route::get('/', 'PelayanController@index');
	 Route::get('/order', 'PelayanController@order');
	 Route::post('order/add', 'PelayanController@store');
	 Route::get('pesanan', 'PelayanController@pesanan');
	 Route::get('pesanan/daftar', 'PelayanController@daftarpesanan');
	 Route::post('/order/detailorder','PelayanController@detailorder');
	 Route::get('/pesanan/daftar/edit/{id}', 'PelayanController@edit');
	 Route::post('/pesanan/daftar/edit/{id}', 'PelayanController@update');
});