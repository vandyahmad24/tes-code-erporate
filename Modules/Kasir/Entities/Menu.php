<?php

namespace Modules\Kasir\Entities;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['name','harga','tipe','status'];
    public function order_detail()
    {
    	return $this->hasMany("Modules\Kasir\Entities\Order_detail");
    }
}
