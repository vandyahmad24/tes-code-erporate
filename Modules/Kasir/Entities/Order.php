<?php

namespace Modules\Kasir\Entities;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['nopesanan','nomeja','total','user_id','status'];

   public function order_detail()
    {
    	return $this->hasMany("Modules\Kasir\Entities\Order_detail");
    }
}
