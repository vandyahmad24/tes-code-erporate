<?php

namespace Modules\Kasir\Entities;

use Illuminate\Database\Eloquent\Model;

class Logactivity extends Model
{
    protected $fillable = ['user_id','aktivityas'];
}
