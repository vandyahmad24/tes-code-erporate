<?php

namespace Modules\Kasir\Entities;

use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    protected $fillable = ['menu_id','order_id','qty','subtotal'];

    public function menu()
    {
    	return $this->belongsTo('Modules\Kasir\Entities\Menu');
    }
    public function order()
    {
    	return $this->belongsTo('Modules\Kasir\Entities\Order');
    }
}
