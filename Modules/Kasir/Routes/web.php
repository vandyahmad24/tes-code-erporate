<?php
use App\Http\Middleware\CheckKasir;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('kasir')->group(function() {
//     Route::get('/', 'KasirController@index');
// })->middleware('checkkasir');

Route::group(['prefix' => 'kasir', 'middleware' => ['checkkasir']], function()
{
	Route::get('/', 'KasirController@index');
	Route::get('/daftarmenu', 'KasirController@showmenu');
	Route::post('/tambahmenu', 'KasirController@storemenu');
	Route::get('/menu/hapus/{id}', 'KasirController@destroy');
	Route::get('/menu/edit/{id}', 'KasirController@edit');
});