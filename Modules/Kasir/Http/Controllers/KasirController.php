<?php

namespace Modules\Kasir\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Carbon\Carbon;
use Session;
use Illuminate\Support\Facades\DB;
use Modules\Kasir\Entities\Order;
use Modules\Kasir\Entities\Logactivity;
use Modules\Kasir\Entities\Menu;
use Modules\Kasir\Entities\Order_detail;

class KasirController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $waktu = Carbon::now()->format('d-m-Y');
        $order = Order::count();
        $totalpendapatan = Order::sum('total');
        $menu = Menu::where('status','ready')->count();
        $menuready = Menu::where('status','ready')->get();
        $menunonready = Menu::where('status','noready')->get();
        return view('kasir::index',compact('waktu','totalpendapatan','menu','order','menuready','menunonready'));
        
    }
    public function showmenu()
    {
       $menu = DB::table('menus')
                ->orderBy('id', 'desc')
                ->get();
        return view('kasir::daftarmenu',compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('kasir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function storemenu(Request $request)
    {
       
        $menu = new Menu;
        $menu->name = $request->name;
        $menu->harga= $request->harga;
        $menu->tipe = $request->tipe;
        $menu->status=$request->status;
        $menu->save();
        return redirect('/kasir/daftarmenu');
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('kasir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $menu = Menu::find($id);
        return view('kasir::edit',compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id)->delete();
        Session::flash("success","Data Anda telah dihapus");
        return redirect('/kasir/daftarmenu');
    }
}
