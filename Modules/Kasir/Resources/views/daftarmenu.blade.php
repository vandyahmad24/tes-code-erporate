@extends('kasir::layouts.template')
@section('title', 'Halaman Kasir')
@section('content')
 		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						
						<div class="panel-body">
							<div class="col-md-12">
                 <div class="white-box">
                    <h2 class="header-title">  @yield('title')</h2>
                     <div class="table-responsive">
                        @if(Session::has("success"))
                  <div class="alert alert-success">
                    {{Session::get('success')}}
                  </div>
                  @endif
                  @if(Session::has("error"))
                  <div class="alert alert-danger">
                    {{Session::get('error')}}
                  </div>
                  @endif
								<div class="col">
									<h3>Daftar Menu yang Ready</h3>
									<button class="btn btn-primary" data-toggle="modal" data-target="#tambahdata">Tambah Menu</button>
									<div class="modal fade" id="tambahdata" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">
									    <div class="modal-content">
									      <div class="modal-header">
									        <h3 class="modal-title" id="exampleModalLabel">Tambah Data Menu</h3>
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
									          <span aria-hidden="true">&times;</span>
									        </button>
									      </div>
									      <div class="modal-body">
									        <form action="/kasir/tambahmenu" method="post">
                                  			{{ csrf_field() }}
									       	<div class="form-group">
											<label for="name">Masukkan Nama Makanan</label>
											<input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama makanan" required>
											</div>
											<div class="form-group">
											<label for="harga">Masukkan Harga Makanan</label>
											<input type="number" class="form-control" id="harga" name="harga" placeholder="Masukan Harga makanan" required>
											</div>
											<div class="form-group">
											<label for="tipe">Tipe</label>
											<select class="form-control" name="tipe">
												<option value="makanan">Makanan</option>
												<option value="minuman">Minuman</option>
											</select>
											</div>
											<div class="form-group">
											<label for="status">Status</label>
											<select class="form-control" name="status">
												<option value="ready">Ready</option>
												<option value="noready">No Ready</option>
											</select>
											</div>
									       
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									        <button type="submit" class="btn btn-primary">Save changes</button>
									      </div>
									      </form>
									    </div>
									  </div>
									</div>
									<br><br>
									 <table class="table table-striped table-bordered">
									    <thead>
									      <tr>
									        <th>Nama Menu</th>
									        <th>Harga</th>
									        <th>Tipe</th>
									        <th width="250px">Status</th>
									        <th width="250px">Action</th>
									      </tr>
									    </thead>
									    <tbody>
									    	 @foreach($menu as $m)
									      <tr>
									        <td>{{$m->name}}</td>
									        <td>{{$m->harga}}</td>
									        <td>{{$m->tipe}}</td>
									       	<td>
							       		  <select class="custom-select @if($m->status == 'noready') label label-warning @endif @if($m->status == 'ready') label label-success @endif" id="status" name="status">
		                                <option class="custom-select label label-warning" value="noready" @if($m->status == 'noready') selected @endif>No Ready</option>
		                                <option class="custom-select label label-success" value="ready" @if($m->status == 'ready') selected @endif>Ready</option>
		                              </select>
									       	</td>
									        <td>
									        	<a href="/kasir/menu/edit/{{$m->id}}" class="btn btn-success">Edit</a>
									        	<a href="/kasir/menu/hapus/{{$m->id}}" class="btn btn-danger">Delete</a>
									        </td>
									      </tr>
									      @endforeach
									    </tbody>
									  </table>
								</div>
							</div>
					
					</div>
					<!-- END OVERVIEW -->					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
@endsection
