@extends('kasir::layouts.template')
@section('title', 'Halaman Kasir')
@section('content')
 		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						
						<div class="panel-body">
							<div class="col-md-12">
								<form>
								  <div class="form-group">
								    <label for="name">Nama Menu</label>
								    <input type="text" class="form-control" name="name" value="{{$menu->name}}">
								  </div>
								   <div class="form-group">
								    <label for="harga">Harga Menu</label>
								    <input type="number" class="form-control" name="harga" value="{{$menu->harga}}">
								  </div>
								  <div class="form-group">
									<label for="tipe">Tipe</label>
									<select class="form-control" name="tipe">
										@foreach($menu as $m)
										<option value="{{$m->tipe}}">{{$m->tipe}}</option>
										@endforeach
									</select>
								  </div>
								  <button type="submit" class="btn btn-primary">Submit</button>
								</form>
							</div>
					
					</div>
					<!-- END OVERVIEW -->					
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
@endsection
